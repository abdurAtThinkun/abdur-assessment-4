﻿using System;
using Android.Views;
using Android.Support.V7.Widget;
using System.Collections.Generic;
namespace AndroidRecyclerViewInfiniteScroll
{
    
        public class ItemAdapter : RecyclerView.Adapter
        {
            public event EventHandler<int> ItemClick;
            List<Item> items;

            void OnItemClick(int position)
            {
                if (ItemClick != null)
                {
                    ItemClick(this, position);
                }
            }

            public ItemAdapter(List<Item> items)
            {
                this.items = items;

            }

            public override int ItemCount
            {

                get
                {
                    return items.Count;
                }
            }

            public override void OnBindViewHolder(RecyclerView.ViewHolder holder, int position)
            {
                var vh = (ItemViewHolder)holder;
                vh.Name.Text = items[position].Name;
                vh.Image.SetImageResource(items[position].PhotoId);


            }

            public override RecyclerView.ViewHolder OnCreateViewHolder(ViewGroup parent, int viewType)
            {
                var layout = LayoutInflater.From(parent.Context).Inflate(Resource.Layout.Item, parent, false);
                return new ItemViewHolder(layout, OnItemClick);
            }
        }
    }

