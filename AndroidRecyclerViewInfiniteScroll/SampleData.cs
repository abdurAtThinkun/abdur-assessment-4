﻿using System;
using System.Collections.Generic;

namespace AndroidRecyclerViewInfiniteScroll
{
    public static class SampleData
    {
        static List<Item> items;
        public static List<Item> GetItems()
        {
            items = new List<Item>();

            //adding items to the list
            for (int i = 1; i < 30; i++)
                items.Add(new Item("Item" + i, Resource.Drawable.Icon));

           return items;
        }

        public static Item GetSingleItem(int position)
        {
            Item i;
            i = items[position];
            return i;
        }
    }
}
