﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Support.V7.Widget;
using Android.Views;
using Android.Widget;

namespace AndroidRecyclerViewInfiniteScroll
{
    [Activity(Label = "OnClickingItem")]
    public class OnClickingItem : Activity
    {
        RecyclerView recyclerView;
        List<Item> clickedItem = new List<Item>();

        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            SetContentView(Resource.Layout.Main);

            int itemPosition = Intent.Extras.GetInt("ItemPosition");

            clickedItem.Add(SampleData.GetSingleItem(itemPosition)); 

            recyclerView = FindViewById<RecyclerView>(Resource.Id.recyclerView);
            recyclerView.SetLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.Vertical, false));
            var adapter = new ItemAdapter(clickedItem);
      
            recyclerView.SetAdapter(adapter);


        }
    }
}
