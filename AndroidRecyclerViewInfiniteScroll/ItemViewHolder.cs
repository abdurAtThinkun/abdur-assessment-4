﻿using System;
using Android.Widget;
using Android.Views;
using Android.Support.V7.Widget;
namespace AndroidRecyclerViewInfiniteScroll
{
    public class ItemViewHolder : RecyclerView.ViewHolder
    {
        public TextView Name { get; set; }
        public ImageView Image { get; set; }

        public ItemViewHolder(View itemView, Action<int> listener) : base(itemView)
        {
            Name = itemView.FindViewById<TextView>(Resource.Id.nameTextView);
            Image = itemView.FindViewById<ImageView>(Resource.Id.imageView);

            itemView.Click += (sender, e) => listener(base.AdapterPosition);
            itemView.Click += (sender, e) => listener(base.LayoutPosition);

        }

    }
}
