﻿using Android.App;
using Android.Widget;
using Android.OS;
using Android.Support.V7.Widget;
using Android.Content;
using System.Collections.Generic;
using System;

namespace AndroidRecyclerViewInfiniteScroll
{
    [Activity(Label = "Android RecyclerView Infinite Scroll", MainLauncher = true, Icon = "@mipmap/icon")]
    public class MainActivity : Activity
    {
        RecyclerView recyclerView;
        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);

            // Set our view from the "main" layout resource
            SetContentView(Resource.Layout.Main);

            recyclerView = FindViewById<RecyclerView>(Resource.Id.recyclerView);
            recyclerView.SetLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.Vertical, false));

            var adapter = new ItemAdapter(SampleData.GetItems());
            adapter.ItemClick += OnItemClick;
            recyclerView.SetAdapter(adapter);
        }

        private void OnItemClick(object sender, int position)
        {

            var onClicking = new Intent(this, typeof(OnClickingItem));
            onClicking.PutExtra("ItemPosition", position);
            StartActivity(onClicking);

        }
    }

}

