﻿using System;
using System.Collections.Generic;
namespace AndroidRecyclerViewInfiniteScroll
{
    public class Item
    {
        public Item(String name, int photoId)
        {
            Name = name;
            PhotoId = photoId;
        }

        public string Name { get; set; }
        public int PhotoId { get; set; }
     }

}
